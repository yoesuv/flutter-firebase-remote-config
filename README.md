## Flutter Firebase Remote Config ##

setup firebase remote config for Flutter app.  
download apk file [here](https://www.dropbox.com/s/dkbppomnok0ckfv)

#### Screenshot ####
| Home Screen |
| :---: |
| ![](https://images2.imgbox.com/7a/cd/sg3xKSSI_o.jpg) |

#### List Library ####
- [bloc library](https://bloclibrary.dev/)
- [equatable](https://pub.dev/packages/equatable)
- [Firebase Core](https://pub.dev/packages/firebase_core)
- [Firebase Remote Config](https://pub.dev/packages/firebase_remote_config)
- [Package Info Plus](https://pub.dev/packages/package_info_plus)

#### References ###
- [Remote Config Docs](https://firebase.google.com/docs/remote-config/get-started)
- [Youtube](https://youtu.be/mPghiKYKUV4)
