import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remote_config/src/core/blocs/home_bloc.dart';
import 'package:remote_config/src/core/states/home_state.dart';

class Home extends StatelessWidget {

  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Home')),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return BlocBuilder<HomeBloc, HomeState>(builder: (context, state){
      final versionAndroid = state.versionAndroid ?? '';
      final versionIOS = state.versionIOS ?? '';
      final versionAndroidLocal = state.versionAndroidLocal ?? '';
      if (state.loading == true) {
        return const Center(
          child: CircularProgressIndicator(),
        );
      }
      return Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const Text('Firebase Remote Config Service', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
            Text('Android Version : $versionAndroid', style: const TextStyle(fontSize: 16)),
            Text('iOS Version : $versionIOS', style: const TextStyle(fontSize: 16)),
            const SizedBox(height: 20),
            const Text('Installed Version', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
            Text('Android Version Installed : $versionAndroidLocal', style: const TextStyle(fontSize: 16)),
          ],
        )
      );
    });
  }

}