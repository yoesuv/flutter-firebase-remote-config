import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';

const String _update_android = "update_android";
const String _update_ios = "update_ios";

class RemoteConfigService {

  final RemoteConfig? _remoteConfig;
  final defaults = <String, dynamic> {_update_android: '1.0.0', _update_ios: '1.0.0'};

  static RemoteConfigService? _instance;
  static RemoteConfigService? getInstance(){
    _instance ??= RemoteConfigService(config: RemoteConfig.instance);
    return _instance;
  }

  RemoteConfigService({RemoteConfig? config}): _remoteConfig = config;

  String? get updateAndroid => _remoteConfig?.getString(_update_android);
  String? get updateIos => _remoteConfig?.getString(_update_ios);

  Future initConfig() async {
    try {
      await _remoteConfig?.setDefaults(defaults);
      await _remoteConfig?.fetch();
      await _remoteConfig?.fetchAndActivate();
    } catch (e) {
      debugPrint('RemoteConfigService # error $e', wrapWidth: 1024);
    }
  }

}