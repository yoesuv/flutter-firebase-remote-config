import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:remote_config/src/core/configs/remote_config_service.dart';
import 'package:remote_config/src/core/events/home_event.dart';
import 'package:remote_config/src/core/states/home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState>{

  HomeBloc(): super(const HomeState()) {
    on<HomeEventLoad>(_loadFirebaseConfig);
  }

  Future<void> _loadFirebaseConfig(HomeEventLoad event, Emitter<HomeState> emit) async {
    final packageInfo = await PackageInfo.fromPlatform();
    var remoteConfigService = await RemoteConfigService.getInstance();
    await remoteConfigService?.initConfig();
    final versionAndroid = remoteConfigService?.updateAndroid;
    final versionIos = remoteConfigService?.updateIos;
    debugPrint('HomeBloc # result firebase config android $versionAndroid/ iOS $versionIos');
    emit(state.copyWith(
      loading: false,
      versionAndroid: versionAndroid,
      versionIOS: versionIos,
      versionAndroidLocal: packageInfo.version,
    ));
  }

}