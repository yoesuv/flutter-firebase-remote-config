import 'package:equatable/equatable.dart';

class HomeState extends Equatable {

  final bool? loading;
  final String? versionAndroid;
  final String? versionIOS;
  final String? versionAndroidLocal;

  const HomeState({this.loading = true, this.versionAndroid, this.versionIOS, this.versionAndroidLocal});

  HomeState copyWith({bool? loading, String? versionAndroid, String? versionIOS, String? versionAndroidLocal}){
    return HomeState(
      loading: loading ?? this.loading,
      versionAndroid: versionAndroid ?? this.versionAndroid,
      versionIOS: versionIOS ?? this.versionIOS,
      versionAndroidLocal: versionAndroidLocal ?? this.versionAndroidLocal,
    );
  }

  @override
  List<Object?> get props => [loading, versionAndroid, versionIOS, versionAndroidLocal];

}