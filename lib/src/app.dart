import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remote_config/src/core/blocs/home_bloc.dart';
import 'package:remote_config/src/core/events/home_event.dart';
import 'package:remote_config/src/ui/home.dart';

class App extends StatelessWidget {

  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Firebase Remote Config',
      theme: ThemeData(
        primarySwatch: Colors.teal,
      ),
      home: BlocProvider(
        create: (context) => HomeBloc()..add(HomeEventLoad()),
        child: const Home(),
      )
    );
  }
}